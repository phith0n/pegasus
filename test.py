is_login = False

def check_login(func):
	def pass_login(*args, **kwargs):
		assert kwargs['username'] == kwargs['password']
		func(*args, **kwargs)
	return pass_login

def not_check_login(func):
	def do_init(*args, **kwargs):
		global is_login
		is_login = True
		func(*args, **kwargs)
	return do_init

class base:
	def __init__(self):
		assert is_login

class welcome(base):
	@not_check_login
	def __init__(self, username):
		base.__init__(self)
		self.u = username

	def show(self):
		print 'hello, %s' % self.u

welcome(username = '123').show()