#coding=utf-8
import tornado.web, time, json
from controller.base import BaseHandler
from util.function import not_need_login, hash
from tornado import gen

class LoginHandler(BaseHandler):
	@not_need_login
	def prepare(self):
		BaseHandler.prepare(self)

	def get(self):
		self.render("login.htm")

	@tornado.web.asynchronous
	@gen.coroutine
	def post(self):
		try:
			username = self.get_argument('username')
			password = self.get_argument('password')
			remember = self.get_argument('remember', default = "off")
			user = yield self.db.member.find_one({"username": username})
			check = yield self.backend.submit(hash.verify, password, user.get("password"))
			if check:
				del user["password"]
				user["login_time"] = time.time()
				user["_id"] = str(user["_id"])
				if remember == "on":
					cookie_json = json.dumps(user)
					self.set_secure_cookie("user_info", cookie_json, expires_days = 30, httponly = True)
				self.session.set("current_user", user)
				self.redirect("/")
			else:
				assert False
		except:
			self.custom_error("用户名或密码错误")

class RegisterHandler(BaseHandler):
	@not_need_login
	def prepare(self):
		BaseHandler.prepare(self)
		self.error = {
			"passworddiff": "两次输入的密码不相同",
		    "usernameused": "用户已经注册过啦",
		    "shortpassword": "密码长度不能少于5个字符",
		}

	def get(self, *args, **kwargs):
		error = self.get_argument("error", default = "")
		error = self.error.get(error)
		self.render("register.htm", error = error)

	@tornado.web.asynchronous
	@gen.coroutine
	def post(self):
		username = self.get_argument("username")
		password = self.get_argument("password")
		repassword = self.get_argument("repassword")
		# 两次输入的密码不匹配
		if password != repassword:
			self.redirect("/register?error=passworddiff")
		# 密码长度太短
		if len(password) < 5:
			self.redirect("/register?error=shortpassword")
		# 加密密码
		password = yield self.backend.submit(hash.get, password)
		member = yield self.db.member.find_one({'username': username})
		# 用户名已存在
		if member:
			self.redirect("/register?error=usernameused")
		#插入用户
		result = yield self.db.member.insert({
			"username": username,
		    "password": password
		})
		self.redirect('/')