__author__ = 'phithon'
import tornado.web, os
from controller.base import BaseHandler
from tornado import gen
from bson.objectid import ObjectId

class UserHandler(BaseHandler):
	def get(self, method):
		method = "%s_act" % method
		if hasattr(self, method):
			getattr(self, method)()
		else:
			self.main_act()

	def quit_act(self):
		if self.get_cookie("user_info"):
			self.clear_cookie("user_info")
		if self.get_cookie("download_key"):
			self.clear_cookie("download_key")
		self.session.delete("current_user")
		self.redirect("/login")

	def main_act(self):
		self.redirect("/")

	@tornado.web.asynchronous
	@gen.coroutine
	def download_act(self):
		key = self.get_argument("key")
		task = yield self.db.task.find_one({
			"_id": ObjectId(key),
			"owner": self.current_user.get("username")
		})
		if task and os.path.exists(task["savepath"]):
			self.set_secure_cookie("download_key", task["savepath"])
			relpath = os.path.relpath(task["savepath"])
			self.redirect("/" + relpath)
		else:
			self.custom_error("File Not Found", status_code = 404)
