#coding=utf-8
import tornado.web
from controller.base import BaseHandler
from tornado import gen
import time, pymongo, os, uuid
from util.pywget import util_download
from util.function import humansize
from bson.objectid import ObjectId

class MainHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	def get(self, page):
		page = page if page and int(page) > 1 else 1
		page = int(page)
		cursor = self.db.task.find({"owner": self.current_user.get("username")})
		cursor.sort([('time', pymongo.DESCENDING)]).limit(5).skip((page - 1) * 5)
		tasks = yield cursor.to_list(length=5)
		self.render("dashboard.htm", tasks = tasks, page = page, relpath = os.path.relpath, getsize = humansize)

	@tornado.web.asynchronous
	@gen.coroutine
	def post(self, *args, **kwargs):
		url = self.get_argument('url')
		filename = self.get_argument('filename')
		now = int(time.time())
		savepath = yield self._get_save_path(now, filename)
		filename = os.path.basename(savepath)
		self.settings.get("rqdb").enqueue_call(func = util_download,
		                                       args = (url, savepath),
		                                       timeout = 600)
		result = yield self.db.task.insert({
			"filename": filename,
		    "url": url,
		    "time": now,
		    "finish": 0,
		    "savepath": savepath,
		    "owner": self.current_user.get("username")
		})
		self.redirect("/")

	@tornado.web.asynchronous
	@gen.coroutine
	def delete(self, *args, **kwargs):
		item = self.get_argument("item")
		result = yield self.db.task.remove(ObjectId(item))
		if result:
			self.write("success")
		else:
			self.write("fail")

	@gen.coroutine
	def _get_save_path(self, t, filename):
		download = os.getcwd() + os.path.sep + self.settings.get("download")
		rootdir = os.path.join(download, time.strftime("%Y/%m/%d", time.localtime(t)))
		rootdir = os.path.abspath(rootdir)

		# if settings.download not in `pwd`
		if not rootdir.startswith(os.getcwd() + os.path.sep):
			raise EOFError
		# if not exists
		if not os.path.exists(rootdir):
			os.makedirs(rootdir)
		# if not dir
		if not os.path.isdir(rootdir):
			raise EOFError

		filename = os.path.basename(filename)
		savepath = os.path.join(rootdir, filename)

		exi = yield self.db.task.find_one({"savepath": savepath})
		if exi and exi["savepath"] == savepath:
			(filename, ext) = os.path.splitext(savepath)
			filename += "_" + uuid.uuid4().get_hex()[0:4]
			savepath = filename + ext

		# if not exists
		# Return absolute path
		# python2.7 test ok , python 3.3 dont need to use raise gen.Return
		if filename:
			raise gen.Return(savepath)
		else:
			filename = uuid.uuid4()
			raise gen.Return(os.path.join(rootdir, filename.get_hex()))