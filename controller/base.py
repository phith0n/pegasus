__author__ = 'phithon'
import tornado.web, json
from extends.torndsession.sessionhandler import SessionBaseHandler

class BaseHandler(SessionBaseHandler):
	def initialize(self):
		self.db = self.settings.get("database")
		self.backend = self.settings.get("thread_pool")

	def prepare(self):
		self.add_header("Content-Security-Policy", "default-src 'none'; script-src 'self' 'unsafe-eval'; "
		                                           "connect-src 'self'; img-src 'self' data:; style-src 'self'; "
		                                           "font-src 'self';")
		self.add_header("X-Frame-Options", "deny")
		self.add_header("X-XSS-Protection", "1; mode=block")
		self.add_header("X-Content-Type-Options", "nosniff")
		self.add_header("x-ua-compatible:", "IE=edge,chrome=1")
		self.clear_header("Server")
		self.check_login()
		#print self.current_user

	def get_current_user(self):
		try:
			user = self.session.get("current_user")
			if not user and self.get_cookie("user_info"):
				scookie = self.get_secure_cookie("user_info")
				user = json.loads(scookie)
				if user.has_key("username"):
					self.session.set("current_user", user)
				else:
					assert False
		except:
			user = None
		return user

	def check_login(self):
		try:
			assert type(self.current_user) is dict
			assert self.current_user.has_key('username')
		except AssertionError:
			if self.get_cookie("user_info"):
				self.clear_cookie("user_info")
			self.redirect('/login')

	def render(self, template_name, **kwargs):
		kwargs["base_url"] = self.settings.get("base_url")
		return super(BaseHandler, self).render(template_name, **kwargs)

	def redirect(self, url, permanent=False, status=None):
		super(BaseHandler, self).redirect(url, permanent, status)
		raise tornado.web.Finish()

	def custom_error(self, info, **kwargs):
		if not self._finished:
			status_code = kwargs.get("status_code", 200)
			self.set_status(status_code)
			self.write("<strong>%s</strong>" % info)
		raise tornado.web.Finish()

class NotFoundHandler(BaseHandler):
	def get(self, *args, **kwargs):
		self.render("404.htm")

	def post(self, *args, **kwargs):
		self.get(*args, **kwargs)