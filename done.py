from rq import Queue
from redis import Redis
import time
from proccess import geturl

# Tell RQ what Redis connection to use
redis_conn = Redis()
q = Queue(connection=redis_conn)  # no args implies the default queue

# Delay execution of count_words_at_url('http://nvie.com')
job = q.enqueue_call(func = geturl,
               args = ('http://www.leavesongs.com/',),
               timeout=30, job_id="job1")
print job.result   # => None

# Now, wait a while, until the worker is finished
time.sleep(2)
print job.result   # => 889