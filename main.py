#!/usr/bin/python
import tornado.ioloop
import tornado.web
import motor, subprocess, rq, redis, sys, os
from concurrent import futures
import controller.base
# http://cachefly.cachefly.net/100mb.test

# mongodb connection
# format: mongodb://user:pass@host:port/
# database name: pdownload
client = motor.MotorClient('mongodb://localhost:27017/')
database = client["pdownload"]

# rqworker process
rqworker = subprocess.Popen("rqworker", stdout = open("/dev/null", "w"))

setting = {
	"base_url":"http://localhost:8888",
    "absolute_path": os.getcwd(),
	"template_path": "templates",
    "cookie_secret": "iAy6iNJ4FPuG",
    "compress_response": True,
    "default_handler_class": controller.base.NotFoundHandler,
    "debug": True,
    "xsrf_cookies": False,
    "static_path": "static",
    "database": database,
    "download": "./download",
    "session": {
	    "driver": "redis",
        "driver_settings": {
	        "host": "localhost",
            "port": 6379,
            "db": 0
        },
		"force_persistence": False,
        "cache_driver": True,
        "cookie_config": {
	        "httponly": True
        }
    },
    "rqworker": rqworker,
    "rqdb": rq.Queue(connection = redis.Redis(host = "localhost", port = 6379, db = 0)),
    "thread_pool": futures.ThreadPoolExecutor(4)
}

application = tornado.web.Application([
	(r"^/login", "controller.auth.LoginHandler"),
	(r"^/register", "controller.auth.RegisterHandler"),
	(r"^/(\d*)", "controller.dashboard.MainHandler"),
	(r"^/user/([a-z]+)", "controller.user.UserHandler"),
    (r"^/download/(.*)", "controller.download.DownloadHandler", {"path": "./download/"})
], **setting)

if __name__ == "__main__":
	try:
		application.listen(8888)
		tornado.ioloop.IOLoop.instance().start()
	except:
		pass
	finally:
		rqworker.kill()
		sys.exit()