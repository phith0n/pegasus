#!/usr/bin/python
# encoding=utf-8
__author__ = 'phithon'
import requests, os, re, pymongo

class wget:
	def __init__(self, config = {}):
		self.config = {
			'block': int(config['block'] if config.has_key('block') else 1024),
		}
		self.total = 0
		self.size = 0
		self.filename = ''
		# 'mongodb://user:pass@localhost:27017/'
		self.client = pymongo.MongoClient('mongodb://localhost:27017/')
		self.db = self.client.pdownload

	def touch(self, filename):
		with open(filename, 'w') as fin:
			pass

	def remove_nonchars(self, name):
		(path, name) = os.path.split(name)
		(name, _) = re.subn(ur'[\\\/\:\*\?\"\<\>\|]', '', name)
		return os.path.join(path, name)

	def support_continue(self, url):
		headers = {
			'Range': 'bytes=0-4'
		}
		try:
			r = requests.head(url, headers = headers)
			crange = r.headers['content-range']
			self.total = int(re.match(ur'^bytes 0-4/(\d+)$', crange).group(1))
			return True
		except:
			pass
		try:
			self.total = int(r.headers['content-length'])
		except:
			self.total = 0
		return False

	def download(self, url, filename, headers = {}):
		finished = False
		block = self.config['block']
		local_filename = self.remove_nonchars(filename)
		size = self.size
		total = self.total