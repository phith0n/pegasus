from redis import Redis
from rq import Queue
from tesk import urlget
import time

q = Queue(connection=Redis())

job = q.enqueue(urlget, 'http://www.leavesongs.com/')
print(job.result)

# Now, wait a while, until the worker is finished
time.sleep(5)
print(job.result)