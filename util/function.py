#coding=utf-8
import bcrypt, os

class hash:
	@staticmethod
	def get(str):
		str = str.encode("utf-8")
		return bcrypt.hashpw(str, bcrypt.gensalt())

	@staticmethod
	def verify(str, hashed):
		str = str.encode("utf-8")
		hashed = hashed.encode("utf-8")
		return bcrypt.hashpw(str, hashed) == hashed

def not_need_login(func):
	def do_prepare(self, *args, **kwargs):
		self.current_user = {'username': 'guest'}
		func(self, *args, **kwargs)
		self.current_user = None
	return do_prepare

def humansize(file):
	if os.path.exists(file):
		nbytes = os.path.getsize(file)
		suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
		if nbytes == 0: return '0 B'
		i = 0
		while nbytes >= 1024 and i < len(suffixes)-1:
			nbytes /= 1024.
			i += 1
		f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
		return '%s %s' % (f, suffixes[i])
	else:
		return u"未知"
